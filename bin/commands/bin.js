const figlet = require('figlet');
const getData = require('./functions/getData')
function bin () {
  figlet.text('BIN Checker', {
    font: 'digital',
    horizontalLayout: 'default',
    verticalLayout: 'default',
    width: 80,
    whitespaceBreak: true
}, function(err, data) {
    if (err) {
        console.log('Something went wrong...');
        console.dir(err);
        return;
    }
    console.log(data);
});

setTimeout(() => {
  getData();
}, 1000);

}
module.exports = bin