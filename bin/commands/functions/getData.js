const conf = new (require('conf'))()
const chalk = require('chalk')
const readline = require('readline');
const axios = require('axios');
const { table } = require('console');
const log = console.log;

function getData () {

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

ask()

function ask(answer) {
  rl.question('Enter a BIN/IIN Code: ' , function (bin) {
    axios.get('https://bin-check-dr4g.herokuapp.com/api/' + bin)
    .then(function (response) {
      console.table(response.data)
      askAgain()
    })
  }) 
}

function askAgain(answer) {
  setTimeout(() => {
    rl.question('Dou you want to check another BIN? (y/n) ', function(answer) {
      if (answer === 'y') {
        while (answer == 'y') {
          ask()
          another()
          return
        }
      } else if (answer === 'n') {
        rl.close()
      } else {
        askAgain()
      }
    }) 
  }, 1000);
}

function another(answer) {
  rl.question('Another? ', function (answer) {
    askAgain()
    return
  })
}

rl.on('close', function () {
  // console.log('\nThank you for using our program');
  log(chalk.black.bgWhite.bold('\nThank you for using our program!'));
  process.exit(0);
});

}
module.exports = getData