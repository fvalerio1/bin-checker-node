#! /usr/bin/env node


const { program } = require('commander');
const list = require('./commands/list')
const bin = require('./commands/bin')

program
    .command('bin')
    .description('Check BIN Code')
    .action(bin)
    program.parse()

